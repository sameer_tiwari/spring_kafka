package com.infostretch.kafka.kafkaproducerExample.controller;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.infostretch.kafka.kafkaproducerExample.model.User;

@RestController
@RequestMapping("kafka")
public class UserController {

    @Autowired
    private KafkaTemplate<String, User> kafkaTemplate;
    
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate2;

    private static final String TOPIC = "kafka-example";

    @GetMapping("/publish/{name}")
    public String post(@PathVariable("name") final String name) {

        kafkaTemplate.send(TOPIC, new User(name, "Technology", 12000L));

        return "Published successfully";
    }
    
    @GetMapping("/send/{name}")
    public String publish(@PathVariable("name") final String name) {

        kafkaTemplate2.send(TOPIC, name);

        return "Published successfully";
    }
    
    
}
